﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class Unity_Ads : MonoBehaviour {

    public Item_Manager item_mng;

    IEnumerator ShowAd()
    {
        //유니티Ads 초기화가 안됬을때 초기화 될때까지 기다림
        while (Advertisement.isInitialized == false)
        {
            yield return null;
        }
        // 인터넷 연결 상태 체크.
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (Advertisement.IsReady())
            {
                string zone = "defaultZone";

                ShowOptions options = new ShowOptions();
                options.resultCallback = HandleShowResult;

                Advertisement.Show(zone, options);
            }
        }
    }

    void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //스킵없이 끝까지 시청했을 때 처리
                item_mng.ShuffleItem();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                //동영상을 스킵했을 때 처리
                item_mng.ShuffleItem();
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                //광고 보기 실패(취소) 처리
                break;
        }
    }
}
