﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Maps : MonoBehaviour {

    public GameObject[] rooms;

    public Transform player_trans;
    public Transform camera_trans;
    public Mini_Map mini_map;

    //GridManager grid_manager;
    //Fade fade;
    Boss_Room boss_room;
    Item_Manager item_mng;
    Player_State player_state;
    public Door[] door_scrt;
    public Set_Object[] set_obj;
    Vector3[] room_xz;  //y값은 depth
    Vector2[] check_pos;
    Vector2 curr_xz;
    int room_count = 0;
    int check = 0;
    public int room_num = 0;

    void Awake()
    {
        Define.Setting();
        GridManager.instance.SendMessage("InitNodes");
        
        player_state = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();

        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();

        room_count = Define.ROOM_NUM - 1;
        door_scrt = new Door[Define.ROOM_NUM];
        set_obj = new Set_Object[Define.ROOM_NUM];
        room_xz = new Vector3[Define.ROOM_NUM];
        check_pos = new Vector2[Define.ROOM_NUM];
        boss_room = rooms[Define.ROOM_NUM - 1].GetComponent<Boss_Room>();

        //SetArray();
        //CreateMap();

        //fade.StartCoroutine("FadeIn");  //밝아지게
        Fade.instance.FadeIn();
    }

    public void Reset()
    {

        room_count = Define.ROOM_NUM - 1;
        room_num = 0;
        check = 0;
        mini_map.Reset();
        for (int i = 0; i < Define.ROOM_NUM; ++i)
        {
            //맵 좌표 초기화
            room_xz[i] = new Vector3(0, 0, 0);
            check_pos[i] = new Vector2(0, 0);

            //적 생존 상태 초기화
            if (i != 0) {
                door_scrt[i].enemy_die = false;
                rooms[i].SetActive(false);
            }
            for (int j = 0; j < set_obj[i].enemy.Length; ++j)
                set_obj[i].enemy[j].SetActive(true);

            //문 상태 초기화
            door_scrt[i].Reset();
        }
    }

    public void Setting()
    {
        //층 갱신
        player_state.Update_ui();
        GridManager.instance.SendMessage("ResetObjNode", new Vector3(-1, -1, -1));
        item_mng.ResetItem();
        SetArray();
        CreateMap();
    }

    //맵배치순서 랜덤
    void SetArray()
    {
        //임시 리스트를 사용해 맵의 배치 순서를 랜덤으로 바꾼다.(첫번째 방, 마지막 방 제외)
        ArrayList temp = new ArrayList();
        for (int i = 1; i < Define.ROOM_NUM - 1; ++i)
        {
            temp.Add(rooms[i]);
        }
        int num = 1;
        while (temp.Count > 0)
        {
            int n = Random.Range(0, temp.Count);
            rooms[num] = (GameObject)temp[n];
            temp.RemoveAt(n);
            num++;
        }

        for (int i = 0; i < Define.ROOM_NUM; ++i)
        {
            door_scrt[i] = rooms[i].GetComponent<Door>();
            set_obj[i] = rooms[i].GetComponent<Set_Object>();
        }
    }

    //시작하는 방 생성
    void CreateMap()
    {
        
        rooms[0].SetActive(true);
        rooms[0].transform.position = new Vector3(0, 0, 0);
        room_xz[0] = new Vector3(0, 0, 0);
        check_pos[check] = new Vector2(0, 0);
        curr_xz = new Vector2(room_xz[0].x, room_xz[0].z);
        mini_map.CreateIcons(0, 0);

        room_count--;
        check++;
        GridManager.instance.SendMessage("CreateObstacles");

        //임시 리스트를 사용해 문의 배치 순서를 랜덤으로 바꾼다.
        ArrayList temp = new ArrayList{ Dir.left, Dir.right, Dir.up, Dir.down};
        int[] door_tag = new int[4];
        int num = 0;
        while (temp.Count > 0)
        {
            int n = Random.Range(0, temp.Count);
            door_tag[num] = (int)temp[n];
            temp.RemoveAt(n);
            num++;
        }
        
        //문 몇개 개방할지 랜덤으로 결정
        int door_num = Random.Range(1, 4);
        room_count -= door_num;
        
        for (int j = 0; j < door_num; ++j)
        {
            door_scrt[0].isDoor[door_tag[j]] = true; //현재 방의 tag방향은 문으로 설정
            door_scrt[0].isOpened[door_tag[j]] = true;   //시작 방의 문은 항상 개방

            //만들고자하는 방향의 공간 예약!
            if (door_tag[j] == 0) check_pos[check] = new Vector2(-1, 0);
            else if (door_tag[j] == 1) check_pos[check] = new Vector2(1, 0);
            else if (door_tag[j] == 2) check_pos[check] = new Vector2(0, 1);
            else if (door_tag[j] == 3) check_pos[check] = new Vector2(0, -1);

            check++;
        }

        //방 만들기!
        for(int j = 0; j < door_num; ++j)
        {
            CreateRoom((Dir)door_tag[j], 0, 0, 1);
        }
        
        door_scrt[0].DrawDoor();
        
    }

    //이어진 방의 문 방향을 받아와서 내 방 위치와 이어지는 문을 설정
    void CreateRoom(Dir num, int x, int z, int depth)
    {
        room_num++;
        
        int my_num = room_num, my_x = x, my_z = z;
        rooms[my_num].SetActive(true);
        
        ArrayList temp = new ArrayList { Dir.left, Dir.right, Dir.up, Dir.down };   //랜덤뽑기용

        
        //이어진 문 방향에 맞춰서 문을 만들고 랜덤뽑기할때 그 방향 뽑지 않게 함 
        switch (num)
        {
            case Dir.left:
                my_x--;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.right] = true;
                room_xz[my_num] = new Vector3(my_x, depth, my_z);
                temp.Remove(Dir.right);
                break;

            case Dir.right:
                my_x++;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.left] = true;
                room_xz[my_num] = new Vector3(my_x, depth, my_z);
                temp.Remove(Dir.left);
                break;

            case Dir.up:
                my_z++;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.down] = true;
                room_xz[my_num] = new Vector3(my_x, depth, my_z);
                temp.Remove(Dir.down);
                break;

            case Dir.down:
                my_z--;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.up] = true;
               room_xz[my_num] = new Vector3(my_x, depth, my_z);
                temp.Remove(Dir.up);
                break;
        }

        //현재 방 미니맵에 그려내기
        mini_map.CreateIcons(my_x, my_z);

        //일반 방 다 만들었으면 가장 멀리있는방 찾고 보스방 생성 후 종료!
        if (room_num == 8) {
            FindMaxDepthRoom();    
            return;
        }

        //현재 기준 4방향의 xz좌표를 저장
        Vector2[] temp_xz = new Vector2[4];
        temp_xz[0] = new Vector2(my_x - 1, my_z);
        temp_xz[1] = new Vector2(my_x + 1, my_z);
        temp_xz[2] = new Vector2(my_x, my_z + 1);
        temp_xz[3] = new Vector2(my_x, my_z - 1);

        //현재 4방향 중 기존에 존재하는 방은 랜덤후보에서 제거
        foreach (Vector2 v in check_pos)
        {
            for(int i = 0; i < 4; ++i )
            {
                if (v == temp_xz[i])
                    temp.Remove((Dir)i);
            }
        }


        //방을 추가로 더 만들어야 한다면, 랜덤후보가 있으면
        if (room_count > 0 && temp.Count > 0)
        {
            //임시 리스트를 사용해 문의 배치 순서를 랜덤으로 바꾼다.
            int[] door_dir = new int[temp.Count];
            int t_num = 0;
            while (temp.Count > 0)
            {
                int n = Random.Range(0, temp.Count);
                door_dir[t_num] = (int)temp[n];
                temp.RemoveAt(n);
                t_num++;
            }

            //문 몇개 개방할지 랜덤으로 결정       
            int max = temp.Count;
            max = Mathf.Clamp(max, 0, room_count);
            int door_num = Random.Range(1, max);
            room_count -= door_num;

            for (int j = 0; j < door_num; ++j)
            {
                door_scrt[my_num].isDoor[door_dir[j]] = true; //현재 방향은 문으로 설정

               //만들고자하는 방향의 공간 예약!
                if (door_dir[j] == 0) check_pos[check] = new Vector2(my_x-1, my_z);
                else if (door_dir[j] == 1) check_pos[check] = new Vector2(my_x+1, my_z);
                else if (door_dir[j] == 2) check_pos[check] = new Vector2(my_x, my_z+1);
                else if (door_dir[j] == 3) check_pos[check] = new Vector2(my_x, my_z - 1);
                check++;
                
            }
            for (int j = 0; j < door_num; ++j)
                CreateRoom((Dir)door_dir[j], my_x, my_z, depth + 1);
        }
    }

    void CreateBossRoom(Dir num, int x, int z)
    {
       
        //보스방 만들기
        int my_num = 9, my_x = x, my_z = z;
        rooms[my_num].SetActive(true);
        

        //이어진 문 방향에 맞춰서 문을 만들고 랜덤뽑기할때 그 방향 뽑지 않게 함 
        switch (num)
        {
            case Dir.left:
                my_x--;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.right] = true;
                room_xz[my_num] = new Vector3(my_x, 0, my_z);
                break;

            case Dir.right:
                my_x++;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.left] = true;
                room_xz[my_num] = new Vector3(my_x, 0, my_z);

                break;

            case Dir.up:
                my_z++;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.down] = true;
                room_xz[my_num] = new Vector3(my_x, 0, my_z);
            break;

            case Dir.down:
                my_z--;
                rooms[my_num].transform.position = new Vector3(my_x * Define.MAPSIZE_W, 0, my_z * Define.MAPSIZE_H);
                door_scrt[my_num].isDoor[(int)Dir.up] = true;
                room_xz[my_num] = new Vector3(my_x, 0, my_z);

                break;
        }

        //현재 방 미니맵에 그려내기
        mini_map.CreateIcons(my_x, my_z);




        //모든 방을 생성-> room_num 초기화, 시작 방을 제외한 모든 방 비활성화
            room_num = 0;
            room_count = Define.ROOM_NUM - 1;

            for (int i = 1; i < Define.ROOM_NUM; ++i)
            {
                rooms[i].SetActive(false);

                //미니맵에 보스방과 보물 방 정보 전송
                if (rooms[i].CompareTag("Treasure_Room"))
                    mini_map.SetSpecialIcon(i, 0);
                else if (rooms[i].CompareTag("Boss_Room"))
                    mini_map.SetSpecialIcon(i, 1);
            }

            mini_map.SetIconsColor(0);  //미니맵 색상
    }

    void FindMaxDepthRoom()
    {
        int max_depth = 0, md_idx = 0;

        //가장 먼 방을 찾는다.
        for (int i = 0; i < 9; ++i)
        {
            if (room_xz[i].y > max_depth)
            {
                md_idx = i;
                max_depth = (int)room_xz[i].y;
            }
        }

        //가장 멀리있는 방 옆에 보스방 생성하기 위해 멀리있는 방에 새로운 문을 만든다.

        //현재 기준 4방향의 xz좌표를 저장
        Vector2[] temp_xz = new Vector2[4];
        temp_xz[0] = new Vector2(room_xz[md_idx].x - 1, room_xz[md_idx].z);
        temp_xz[1] = new Vector2(room_xz[md_idx].x + 1, room_xz[md_idx].z);
        temp_xz[2] = new Vector2(room_xz[md_idx].x, room_xz[md_idx].z + 1);
        temp_xz[3] = new Vector2(room_xz[md_idx].x, room_xz[md_idx].z - 1);

        ArrayList temp = new ArrayList { Dir.left, Dir.right, Dir.up, Dir.down };

        //현재 4방향 중 기존에 존재하는 방은 랜덤후보에서 제거
        foreach (Vector2 v in check_pos)
        {
            for (int i = 0; i < 4; ++i)
            {
                if (v == temp_xz[i])
                    temp.Remove((Dir)i);
            }
        }

        //임시 리스트를 사용해 문의 배치 순서를 랜덤으로 바꾼다.
        int[] door_dir = new int[temp.Count];
        int t_num = 0;
        while (temp.Count > 0)
        {
            int n = Random.Range(0, temp.Count);
            door_dir[t_num] = (int)temp[n];
            temp.RemoveAt(n);
            t_num++;
        }

        door_scrt[md_idx].isDoor[door_dir[0]] = true; //현재 방향은 문으로 설정
                                                      
        if (door_dir[0] == 0) new Vector2(room_xz[md_idx].x - 1, room_xz[md_idx].z);
        else if (door_dir[0] == 1) new Vector2(room_xz[md_idx].x + 1, room_xz[md_idx].z);
        else if (door_dir[0] == 2) new Vector2(room_xz[md_idx].x, room_xz[md_idx].z + 1);
        else if (door_dir[0] == 3) new Vector2(room_xz[md_idx].x, room_xz[md_idx].z - 1);
        check++;

        CreateBossRoom((Dir)door_dir[0], (int)room_xz[md_idx].x, (int)room_xz[md_idx].z);
    }

    public void MoveMap(int num)
    {
        print("MOVEMAP " + (Dir)num);
        GridManager.instance.SendMessage("ResetObjNode", new Vector3(-1, -1, -1));

        mini_map.MoveIcons(num);

        switch ((Dir)num)
        {            
            case Dir.up:
                this.transform.position += Vector3.back * Define.MAPSIZE_H;
                player_trans.position = Define.door_pos[(int)Dir.down];//new Vector3(23, 0, 1);
                GridManager.instance.CalculateObj(player_trans.position, ObjType.character);
                player_trans.eulerAngles = new Vector3(0, 270, 0);
                camera_trans.position = new Vector3(Define.HCENTER, Define.C_POS_Y, Define.C_VMIN - Define.VCENTER+ Define.C_POS_Z);
                curr_xz += Vector2.up;
                break;

            case Dir.down:
                this.transform.position += Vector3.forward * Define.MAPSIZE_H;
                player_trans.position = Define.door_pos[(int)Dir.up]; //new Vector3(23, 0, 23);
                GridManager.instance.CalculateObj(player_trans.position, ObjType.character);
                player_trans.eulerAngles = new Vector3(0, 90, 0);
                camera_trans.position = new Vector3(Define.HCENTER, Define.C_POS_Y, Define.C_VMAX - Define.VCENTER+ Define.C_POS_Z);
                curr_xz += Vector2.down;
                break;

            case Dir.left:
                this.transform.position += Vector3.right * Define.MAPSIZE_W;
                player_trans.position = Define.door_pos[(int)Dir.right]; //new Vector3(41, 0, 13);
                GridManager.instance.CalculateObj(player_trans.position, ObjType.character);
                player_trans.eulerAngles = new Vector3(0, 180, 0);
                camera_trans.position = new Vector3(Define.C_HMAX, Define.C_POS_Y, Define.C_POS_Z);
                curr_xz += Vector2.left;
                break;

            case Dir.right:
                this.transform.position += Vector3.left * Define.MAPSIZE_W;
                player_trans.position = Define.door_pos[(int)Dir.left]; //new Vector3(3, 0, 13);
                GridManager.instance.CalculateObj(player_trans.position, ObjType.character);
                player_trans.eulerAngles = new Vector3(0, 0, 0);
                camera_trans.position = new Vector3(Define.C_HMIN, Define.C_POS_Y, Define.C_POS_Z);
                curr_xz += Vector2.right;
                break;
        }
        //움직인 곳에 있는 방 활성화, 지나온 방 비활성화
        rooms[room_num].SetActive(false);
        item_mng.HideItem(room_num);

        for (int i = 0; i < Define.ROOM_NUM; ++i)
        {
            if (curr_xz.x == room_xz[i].x && curr_xz.y == room_xz[i].z)
            {
                room_num = i;
                mini_map.SetIconsColor(room_num);
                door_scrt[room_num].HideDoor();
                door_scrt[room_num].DrawDoor();
                rooms[room_num].SetActive(true);
                item_mng.ShowItem(room_num);
                GridManager.instance.SendMessage("CreateObstacles");

                if (!door_scrt[i].enemy_die)
                {
                    if (0 < i && i < Define.ROOM_NUM - 1)
                        set_obj[i].CallEnemy();
                    else if (i == Define.ROOM_NUM - 1)
                        boss_room.CallBoss();
                }
                return;
            }
        }
    }
}
