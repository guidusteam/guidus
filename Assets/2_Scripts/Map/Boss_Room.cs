﻿using UnityEngine;
using System.Collections;

public class Boss_Room : MonoBehaviour {

    public GameObject stair;
    public Door door;
    public GameObject[] boss;
    public Maps maps;

    Player_Animate player_animate;
    Player_State player_state;
    Transform player_trans;
    Caravan caravan;

    public Mini_Map minimaps;

    public Camera main_camera;

    Ray ray;
    RaycastHit hit;

    void Start()
    {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        player_trans = GameObject.Find("Player").GetComponent<Transform>();
        caravan = GameObject.Find("Manager").GetComponent<Caravan>();
    }

    void Update()
    {
        if (CheckBoss())
        {
            stair.SetActive(true);
            door.SendMessage("EnemyDie");
        }
        GetEvent();
    }
   
    void GetEvent()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {         
                if (hit.collider.tag == "Stair" &&
                    player_trans.position.x >= 9 && player_trans.position.x <= 13 &&
                    player_trans.position.z >= 21)
                {
                    stair.SetActive(false);

                    this.gameObject.SetActive(false);

                    player_animate.Setting(true);
                   // minimaps.SendMessage("Reset");
                    player_state.floor--;

                    caravan.enabled = true;
                    caravan.StartCoroutine("GotoCaravan");
                    maps.SendMessage("Reset");
                }
            }
        }           
    }

    public void CallBoss(){
        for (int i = 0; i < boss.Length; ++i)
        {
            boss[i].SetActive(true);
            boss[i].SendMessage("SetTarget");
        }
    }

    public bool CheckBoss()
    {
        for (int i = 0; i < boss.Length; ++i)
        {
            if (boss[i].activeSelf)
                return false;
        }
        return true;
    }

    public void KillBoss()
    {

      for (int i = 0; i < boss.Length; ++i)
          boss[i].SetActive(false);
    }
}
