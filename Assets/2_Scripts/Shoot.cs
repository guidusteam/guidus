﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Shoot : MonoBehaviour
{

    public bool active = false;
    public Animator player_action;
    public Player_State player_state;
    public GameObject[] Bullet;
    CapsuleCollider[] bullet_collider;
    Transform[] bullet_trans;
    MeshRenderer[] bullet_draw;
    Bullet[] bullet_scrt;
    TrailRenderer[] trail;
    public Transform player;

    //==============================
    int bullet_num = 10;
    public int bullet_dist = 50;
    public int curve_dist = 3;
    public int curve = 4;
    public int shoot_num = 1;
    //==============================
    float shoot_time = 0;

    Vector3 dest_pos, dest_rot;
    Quaternion look_rot;

    public Camera main_camera;
    Ray ray;
    RaycastHit hit;
    void Start()
    {
        bullet_collider = new CapsuleCollider[bullet_num];
        bullet_trans = new Transform[bullet_num];
        bullet_draw = new MeshRenderer[bullet_num];
        bullet_scrt = new Bullet[bullet_num];
        trail = new TrailRenderer[bullet_num];

        for (int i = 0; i < bullet_num; ++i)
        {
            bullet_collider[i] = Bullet[i].GetComponent<CapsuleCollider>();
            bullet_scrt[i] = Bullet[i].GetComponent<Bullet>();
            bullet_trans[i] = Bullet[i].GetComponent<Transform>();
            bullet_draw[i] = Bullet[i].GetComponent<MeshRenderer>();
            trail[i] = Bullet[i].GetComponent<TrailRenderer>();
        }
    }


    void Update()
    {
        if (active)
            GetEvent();
    }

    public void ShootBullet(float value)
    {
        if (Time.time - shoot_time > 0.7f)
        {
            shoot_time = Time.time;
            player_action.CrossFade("attack", 0.1f);
            player_action.SetBool("shoot", true);

            dest_rot = new Vector3(0, value * 180 + 90, 0);
            player.transform.DORotate(dest_rot, 0.3f);

            for (int i = 0; i < bullet_num; ++i)
            {
                if (bullet_draw[i].enabled == false)
                {
                    CurveShoot(i);
                    return;
                }
            }
        }
    }

    void GetEvent()
    {/*
        if (Input.GetMouseButtonUp(0))
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                player_action.CrossFade("attack", 0.1f);
                player_action.SetBool("shoot", true);

                //마우스 방향으로 회전
                dest_pos = new Vector3(hit.point.x, 0, hit.point.z);
                look_rot = Quaternion.LookRotation(dest_pos - player.position);
                dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;
                player.transform.DORotate(dest_rot, 0.3f);

                //탄환 발사
                for (int i = 0; i < bullet_num; ++i)
                {
                    if (bullet_draw[i].enabled == false)
                    {
                        CurveShoot(i);
                        return;
                    }
                }
            }
        }*/
      
        //터치
        if (Input.touchCount > 1 && Input.GetTouch(0).phase != TouchPhase.Canceled)
        {
            Touch touch = Input.GetTouch(1);

            //터치 시작, 드래그 등 터치하는 모든 상황, 0.7초 간격
            if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled
                && Time.time - shoot_time > 0.7f)
            {
                ray = main_camera.ScreenPointToRay(touch.position);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    player_action.CrossFade("attack", 0.1f);
                    player_action.SetBool("shoot", true);

                    //마우스 방향으로 회전
                    dest_pos = new Vector3(hit.point.x, 0, hit.point.z);
                    look_rot = Quaternion.LookRotation(dest_pos - player.position);
                    dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;
                    player.transform.DORotate(dest_rot, 0.3f);

                    //탄환 발사
                    for (int i = 0; i < bullet_num; ++i)
                    {
                        if (bullet_draw[i].enabled == false)
                        {
                            CurveShoot(i);
                            return;
                        }
                    }
                }
            }
        
        }
    }

    void LinearShoot(int num)
    {
        bullet_draw[num].enabled = true;
        bullet_collider[num].enabled = true;
        trail[num].enabled = true;
        bullet_trans[num].DOLocalMove(new Vector3(bullet_dist,0,0), player_state.shoot_speed).SetEase(Ease.Linear).OnComplete(() => Reset(num));
        player_action.SetBool("shoot", false);
    }

    void CurveShoot(int num)
    {
        //경로 찍기
        Vector3[] path = new Vector3[bullet_dist / curve_dist];

        for (int i = 0; i < bullet_dist / curve_dist; ++i)
        {
            if (i % 2 == 0) path[i] = new Vector3(curve_dist * i, 0, curve);
            else path[i] = new Vector3(curve_dist * i, 0, -curve);
        }
        
        bullet_draw[num].enabled = true;
        bullet_collider[num].enabled = true;
        trail[num].enabled = true;
        bullet_trans[num].DOLocalPath(path, player_state.shoot_speed,PathType.CatmullRom).SetEase(Ease.Linear).OnComplete(()=>Reset(num));      
        player_action.SetBool("shoot", false);
    }

    void Reset(int b_num)
    {
        bullet_scrt[b_num].Reset();
    }
}