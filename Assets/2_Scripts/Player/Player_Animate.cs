﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Player_Animate : MonoBehaviour {

    public Camera main_camera;

    public Transform camera_trans;
    public Animator player_action;
    public ArrayList pathArray;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ParticleSystem start_eff;
    public Maps maps;

    Caravan caravan;
    int[] obs_index;

    Player_State player_state;
    BoxCollider player_collider;
    NPC_Work npc_work;

    Vector3 dest_rot;
    public Vector3 dest_pos;

    public bool is_active;
    Quaternion look_rot;
    bool double_click = false;  //더블클릭 판별
    //bool new_path = false;  //새로운 경로가 주어졌는지


    int rolling_length = 7;
    float rolling_speed = 0.25f;
    int click_num = 0;
    int move_num = 1;
    float[] time = { -1, -1 };
    Ray ray;
    RaycastHit hit;
    void Start()
    {
        obs_index = new int[GridManager.instance.obstacleList.Length];
        player_state = GetComponent<Player_State>();
        player_collider = GetComponent<BoxCollider>();
        caravan = GameObject.Find("Manager").GetComponent<Caravan>();
        npc_work = GameObject.Find("Manager").GetComponent<NPC_Work>();
    }

	void UnFreeze () {

        player_action.enabled = true;

        pathArray = new ArrayList();
        Setting(true);
	}

    void Freeze()
    {
        player_action.enabled = false;
        is_active = false;
    }
    
    public void Setting(bool first = false)
    {
        transform.DOKill();
        transform.position = new Vector3(Define.HCENTER, 0, Define.VCENTER);
        transform.eulerAngles = new Vector3(0, 90, 0);
        GridManager.instance.CalculateObj(transform.position,ObjType.character);

        camera_trans.DOKill();
        camera_trans.position = new Vector3(Define.HCENTER, Define.C_POS_Y, Define.C_POS_Z);
        
        is_active = true;
        pathArray.Clear();
        
        player_action.SetBool("run", false);
        player_action.SetBool("rolling", false);        
        
        dest_pos = transform.position;

        obs_index = new int[GridManager.instance.obstacleList.Length];
        for (int i = 0; i < GridManager.instance.obstacleList.Length; ++i)
            obs_index[i] = GridManager.instance.GetGridIndex(GridManager.instance.obstacleList[i].transform.position);

        if (first)
        {
            start_eff.transform.position = transform.position;
            start_eff.Play();
        }

    }

    public void Reset(bool move = false, int dir = 0)
    {
        transform.DOKill();
        camera_trans.DOKill();
        pathArray.Clear();
        move_num = 1;
        player_action.CrossFade("idle", 0.3f);
        player_action.SetBool("run", false);
        player_collider.enabled = true;

        if (move)   //맵 이동 전 리셋인 경우
            maps.MoveMap(dir);
    }


    void Update () {
        if (is_active)
        {
            GetEvent();
        }
	}

    void GetEvent() {
        if(Input.GetMouseButtonDown(0)) //터치
        {
            ray = main_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")) ||
                Physics.Raycast(ray,out hit, Mathf.Infinity,1 << LayerMask.NameToLayer("Door")))
            {
                //UI터치는 무시, 문 터치는 Door스크립트에서 처리
                return;
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Floor"))) 
            {
                //번갈아가면서 저장할 수 있게
                click_num = (click_num + 1) % 2;

                //시간비교해서 더블클릭 판별
                time[click_num % 2] = Time.time;
                float delta_time = Mathf.Abs(time[0] - time[1]);
                if (delta_time < 0.2f)
                    double_click = true;


                dest_pos = new Vector3(hit.point.x, this.transform.position.y, hit.point.z);

                //가까운 정수로 변환
                dest_pos.x = Mathf.Round(dest_pos.x);
                dest_pos.z = Mathf.Round(dest_pos.z);

                //짝
                if (dest_pos.x % 2 == 0)
                    dest_pos.x -= 1;
                if (dest_pos.z % 2 == 0)
                    dest_pos.z -= 1;

     
                //최대 최소에 맞춰 범위 수정
                dest_pos.x = Mathf.Clamp(dest_pos.x, 1, Define.HMAX -1);
                dest_pos.z = Mathf.Clamp(dest_pos.z, 1, Define.VMAX -1);

                //더블클릭일때 스태미너값 비교해서 실행판단
                if (double_click && player_state.stamina >= 10)
                {
                    player_action.CrossFade("rolling", 0.3f);
                    Rolling();
                    return;
                }
                else if (double_click && player_state.stamina < 10)
                {
                    double_click = false;
                    return;
                }

                FindPath();

            }
        }
    }

    public void FindPath()
    {
        //장애물을 목표위치로 잡았을 경우는 실행하지 않게함
        for (int i = 0; i < obs_index.Length; ++i)
            if (GridManager.instance.GetGridIndex(dest_pos) == obs_index[i])
                return;

        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(transform.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(dest_pos)));

        pathArray = AStar.FindPath(startNode, goalNode);
        move_num = 1;

        Move();
    }

    void Move()
    {
        //제자리 or 모두 이동
        if (move_num >= pathArray.Count)
        {
            Reset();
            return;
        }
        
        Node node = (Node)pathArray[move_num];

        //바로옆이 npc일때 움직이지 않게
        if (node.objtype == ObjType.NPC)
        {
            //회전값
            Quaternion look = Quaternion.LookRotation(dest_pos - this.transform.position);
            Vector3 dest = look.eulerAngles; dest.y -= 90;
            this.transform.DORotate(dest, player_state.move_speed);
            ContactNPC();
            pathArray.Clear();
            return;
        }

        float speed = player_state.move_speed;
        player_action.SetBool("run", true);
        

        //대각선으로 움직일 경우 속도를 같게하기 위해
        float move_length = (node.position - transform.position).magnitude;
        if (move_length > 2)
            speed *= (move_length / 2);

      
        //마지막 노드 동작완료 후 리셋
        if (move_num == pathArray.Count - 1 )
        {
            bool move_map = false;
            int dir = 0;

            //목적지가 문이였을 경우 바로 맵 이동
            for (int i = 0; i < Define.door_count; ++i)
                if (node.position == Define.door_pos[i] && node.flrtype == FlrType.door)
                {
                    move_map = true;
                    dir = i;
                }

            //caravan 출구
            if(node.flrtype == FlrType.exit)
            {
                caravan.StartCoroutine("StartGame");
            }
            
            this.transform.DOKill();

            if (move_map)
                this.transform.DOMove(node.position, speed).SetEase(Ease.Linear).
                OnComplete(() => Reset(move_map,dir));
            else
                this.transform.DOMove(node.position, speed).SetEase(Ease.Linear).
                OnComplete(() => Reset());
           
        }
        //npc터치
        else if (move_num == pathArray.Count - 2 && ((Node)pathArray[move_num + 1]).objtype == ObjType.NPC)   
        {
            this.transform.DOKill();

            this.transform.DOMove(node.position, speed).SetEase(Ease.Linear).
                OnComplete(() => ContactNPC());
        }

        //기본 이동
        else
        {
            this.transform.DOKill();
            this.transform.DOMove(node.position, speed).SetEase(Ease.Linear).
                OnComplete(()=> Move());
        }

        //카메라!!!
        camera_trans.DOKill();
        if (node.position.x >= Define.C_HMIN && node.position.x <= Define.C_HMAX)
        {  
            camera_trans.DOMoveX(node.position.x, speed).SetEase(Ease.Linear);
        }
        if (node.position.z >= Define.C_VMIN && node.position.z <= Define.C_VMAX)
        {
            camera_trans.DOMoveZ(node.position.z - Define.VCENTER+ Define.C_POS_Z, speed).SetEase(Ease.Linear);
        }

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;
        this.transform.DORotate(dest_rot, speed);


        //캐릭터 노드 이동
        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        GridManager.instance.CalculateObj(node.position, ObjType.character);

        ++move_num;       
    }
    
    void Rolling()
    {
        pathArray.Clear();
        move_num = 1;
        this.transform.DOKill();
        
        player_collider.enabled = false;

        //마우스 방향으로 회전
        look_rot = Quaternion.LookRotation(dest_pos - this.transform.position);
        dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;
        this.transform.DORotate(dest_rot, rolling_speed);

        //현재위치->목표위치 방향벡터 정규화
        dest_pos -= this.transform.position;
        dest_pos.Normalize();

        

        //해당 방향으로 7만큼 이동
        dest_pos = this.transform.position + dest_pos * rolling_length;

        if (1 > dest_pos.x || dest_pos.x > Define.HMAX - 1 ||
                         1 > dest_pos.z || dest_pos.z > Define.VMAX - 1)
        {
            dest_pos.x = Mathf.Clamp(dest_pos.x, 1, Define.HMAX - 1);
            dest_pos.z = Mathf.Clamp(dest_pos.z, 1, Define.VMAX - 1);

        }
        dest_pos = GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(dest_pos));
        transform.DOMove(dest_pos, rolling_speed).SetEase(Ease.OutCubic).OnComplete(() => Reset());



        //카메라이동
        camera_trans.DOKill();
        dest_pos.x = Mathf.Clamp(dest_pos.x, Define.C_HMIN, Define.C_HMAX);
        camera_trans.DOMoveX(dest_pos.x, rolling_speed);

        if (dest_pos.z <= Define.C_VMIN)
            camera_trans.DOMoveZ(Define.C_VMIN-Define.VCENTER + Define.C_POS_Z, rolling_speed);
        else if (dest_pos.z >= Define.C_VMAX)
            camera_trans.DOMoveZ(Define.C_VMAX - Define.VCENTER+ Define.C_POS_Z, rolling_speed);

        double_click = false;
        player_state.Update_st(-10);
    }

    void ContactNPC()
    {
        transform.DOKill();
        camera_trans.DOKill();
        
        pathArray.Clear();
        move_num = 1;

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(dest_pos - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles; dest_rot.y -= 90;
        this.transform.DORotate(dest_rot, player_state.move_speed);

        player_action.CrossFade("idle", player_state.move_speed);
        player_action.SetBool("run", false);
        player_collider.enabled = true;

        //contact할 npc 찾기
        for(int i = 0; i < caravan.npc.Length; ++i)
            if(GridManager.instance.GetNodeRowCol(dest_pos) ==
                GridManager.instance.GetNodeRowCol(caravan.npc[i].gameObject.transform.position))
                npc_work.StartCoroutine("Work", caravan.npc[i].mytype);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.magenta);
                    index++;
                }
            };
        }
    }
}
