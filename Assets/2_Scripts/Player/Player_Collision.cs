﻿using UnityEngine;
using System.Collections;

public class Player_Collision : MonoBehaviour {

    Player_State player_state;
    Item_Manager item_mng;

    void Start()
    {
        player_state = GetComponent<Player_State>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
    }

    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Worm_Bullet":
                player_state.Update_hp(-3);
                break;

            case "Coin":
                player_state.Update_coin(10);
                item_mng.HideItem(other.transform.position);
                break;

            case "HP":
                player_state.Update_hp(10);
                item_mng.HideItem(other.transform.position);
                break;
        }
    }
}
