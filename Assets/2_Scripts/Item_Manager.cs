﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Item_Manager : MonoBehaviour
{
    Player_State player_state;
    Player_Animate player_animate;
    public Text item_name;
    public GameObject button_get, button_retry;
    public GameObject item_window;
    
    public Item[] d_coin, d_hp;
    GameObject[] bullet;
    Manager manager;
    public Treasure_Box treasure_box;
    Maps maps; 

    int item_idx = 0;

    ArrayList items;

    void Start()
    {
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();
        manager = GameObject.Find("Manager").GetComponent<Manager>();
        bullet = GameObject.FindGameObjectsWithTag("Bullet");
        maps = GameObject.Find("Maps").GetComponent<Maps>();

        items = new ArrayList();
        for (int i = 0; i < Define.ITEM_NUM; ++i)
            items.Add((ItemType)i);
    }

    public void ResetItem()
    {
        items.Clear();
        for (int i = 0; i < Define.ITEM_NUM; ++i)
            items.Add((ItemType)i);

        button_retry.SetActive(true);
        button_get.transform.localPosition = new Vector3(-41, 19, 0);

        for (int i = 0; i < d_coin.Length; ++i)
        {
            d_coin[i].Reset();
            d_hp[i].Reset();
        }
    }

    public IEnumerator CreateItem()
    {
        //랜덤한 아이템 뽑기
        item_idx = Random.Range(0, items.Count - 1);

        if (treasure_box.action.IsPlaying("Close"))
            treasure_box.action.CrossFade("Open");

        switch ((ItemType)items[item_idx])
        {
            case ItemType.max_hp_inc://최대 체력 증가
                item_name.text = "최대 체력 증가!";
                break;

            case ItemType.att_power_inc://공격력 증가
                item_name.text = "공격력 증가!";              
                break;

            case ItemType.move_spd_inc:// 속도 증가
                item_name.text = "이동 속도 증가!";
                break;

            case ItemType.max_stam_inc://최대 스테미너 증가
                item_name.text = "최대 스테미너 증가!";
                break;
        }

        yield return new WaitForSeconds(1.0f);

        //창띄우기
        item_window.SetActive(true);
        player_animate.is_active = false;
    }
    
    public void Retry()
    {
        //상자도 닫고 이펙트 끄고 아이템 창 닫고광고보고 오기이
        treasure_box.action.CrossFade("Close", 0.1f);
        treasure_box.open_eff.Stop();
        item_window.SetActive(false); 
        manager.SendMessage("ShowAd");
    }

    public void GetItem()
    {
        item_window.SetActive(false);
        player_animate.is_active = true;
        treasure_box.StartCoroutine("CloseBox");

        switch ((ItemType)items[item_idx])
        {
            case ItemType.max_hp_inc://최대 체력 증가
                player_state.changed.max_health = 10;
                player_state.Update_hp();
                break;

            case ItemType.att_power_inc://공격력 증가
                player_state.changed.attack_power = 5;
                player_state.Update_ui();
                //총알크기
                for (int i = 0; i < bullet.Length; ++i)
                    bullet[i].transform.localScale = Vector3.one * (bullet[i].transform.localScale.x + 0.05f);
                break;

            case ItemType.move_spd_inc:// 속도 증가
                player_state.changed.move_speed = -0.05f;
                player_state.Update_ui();
                break;

            case ItemType.max_stam_inc://최대 스테미너 증가
                player_state.changed.max_stamina = 5;
                player_state.Update_st();
                break;
        }
        
        ResetItem();
    }

    public void ShuffleItem()
    {
        //기존에 받은 아이템은 제외
        items.RemoveAt(item_idx);

        //RETRY 없애기
        button_retry.SetActive(false);

        //중앙으로
        button_get.transform.localPosition = new Vector3(0, 19, 0);

        // 다시 애니메이션+이펙트
        treasure_box.action.CrossFade("Open", 0.1f);
        treasure_box.open_eff.Play();

        //새로 뽑기!
        StartCoroutine("CreateItem");
    }

    public void DropItem(Vector3 pos)
    {
        DItemType drop;

        int rand = Random.Range(0, 10000);
        if (rand < 5000) drop = DItemType.coin;
        else drop = DItemType.hp;

        switch (drop)
        {
            case DItemType.coin:
                //pos에 drop
                for(int i = 0; i < d_coin.Length; ++i)
                {
                    if(d_coin[i].FindByNum())  //쓰지 않은 아이템
                    {
                        d_coin[i].SetPos(pos);
                        d_coin[i].SetRoom(maps.room_num);
                        d_coin[i].Show();
                        return;
                    }
                }
                break;
          
            case DItemType.hp:
                for (int i = 0; i < d_hp.Length; ++i)
                {
                    if (d_hp[i].FindByNum())  //쓰지 않은 아이템
                    {
                        d_hp[i].SetPos(pos);
                        d_hp[i].SetRoom(maps.room_num);
                        d_hp[i].Show();
                        return;
                    }
                }
                break;
        }
    }

    public void HideItem(int room_num)
    {
       for(int i = 0; i < d_coin.Length; ++i)
       {
            if (d_coin[i].FindByNum(room_num))
                d_coin[i].Hide();
            if (d_hp[i].FindByNum(room_num))
                d_hp[i].Hide();
       }
    }

    public void HideItem(Vector3 pos)
    {
        for (int i = 0; i < d_coin.Length; ++i)
        {
            if (d_coin[i].FindByPos(pos))
                d_coin[i].Hide();
            if (d_hp[i].FindByPos(pos))
                d_hp[i].Hide();
        }
    }

    public void ShowItem(int room_num)
    {
        for (int i = 0; i < d_coin.Length; ++i)
        {
            if (d_coin[i].FindByNum(room_num))
                d_coin[i].Show();
            if (d_hp[i].FindByNum(room_num))
                d_hp[i].Show();
        }
    }
}


