﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Button_Shoot : MonoBehaviour {

    public Camera main_camera;
    public Player_Animate player_animate;
    public Shoot shoot;
    public Animator player_action;
    public Collider[] button;
    public Scrollbar left, right;

    Ray ray;
    RaycastHit hit;
    

	void Update ()
    {
        GetEvent();
	}


    void GetEvent()
    {
        /*
        if( Input.GetKeyDown(KeyCode.Space))
        {
            if (hit.collider == button[0])
                button[1].enabled = false;
            else
                button[0].enabled = false;
            player_animate.pathArray.Clear();
            player_animate.is_active = false;
            player_action.SetBool("run", false);
            player_action.SetBool("shoot_button", true);
            shoot.active = true;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            if (hit.collider == button[0])
                button[1].enabled = true;
            else
                button[0].enabled = true;
            player_action.SetBool("shoot_button", false);
            player_animate.is_active = true;
            shoot.active = false;
        }
        */

        /*
        if (Input.touchCount > 0 )
        {
            Touch touch = Input.GetTouch(0);
            ray = main_camera.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")))
            {
                if (hit.collider.CompareTag("Button_Shoot"))
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            if (hit.collider == button[0])
                                button[1].enabled = false;
                            else
                                button[0].enabled = false;
                            player_animate.pathArray.Clear();
                            player_action.SetBool("run", false);
                            player_action.SetBool("shoot_button", true);
                            player_animate.is_active = false;
                            shoot.active = true;
                            break;

                        case TouchPhase.Ended:
                            if (hit.collider == button[0])
                                button[1].enabled = true;
                            else
                                button[0].enabled = true;
                            player_action.SetBool("shoot_button", false);
                            player_animate.is_active = true;
                            shoot.active = false;
                            break;
                    }

                }

            }
        }
        */

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            ray = main_camera.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("UI")))
            {
                if (hit.collider.CompareTag("Button_Shoot"))
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            
                            player_animate.pathArray.Clear();
                            player_action.SetBool("run", false);
                            player_action.SetBool("shoot_button", true);
                            player_animate.is_active = false;
                            shoot.active = true;
                            if (hit.collider == left)
                            {
                                right.enabled = false;
                                shoot.ShootBullet(left.value);
                            }
                            else
                            {
                                left.enabled = false;
                                shoot.ShootBullet(-right.value);
                            }
                            break;

                        case TouchPhase.Moved:
                            if (hit.collider == left)
                            {
                                right.enabled = false;
                                shoot.ShootBullet(left.value);
                            }
                            else
                            {
                                left.enabled = false;
                                shoot.ShootBullet(-right.value);
                            }
                            break;

                        case TouchPhase.Ended:
                            if (hit.collider == left)
                                right.enabled = false;
                            else
                                left.enabled = false;
                            player_action.SetBool("shoot_button", false);
                            player_animate.is_active = true;
                            shoot.active = false;
                            break;
                    }

                }

            }
        }
    }
    
}
