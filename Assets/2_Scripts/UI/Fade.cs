﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Fade : MonoBehaviour {

    public Image fade_scr;
    private static Fade s_Instance = null;

    public static Fade instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = FindObjectOfType(typeof(Fade)) as Fade;
                if (s_Instance == null)
                    Debug.Log("Could not locate an Fade object. \n You have to have exactly one Fade in the scene.");
            }
            return s_Instance;
        }
    }

    
    public void FadeIn(float fade_time = 0.5f)
    {
        fade_scr.enabled = true;
        fade_scr.color = new Vector4(0, 0, 0, 1);
        fade_scr.DOFade(0, fade_time);
    }

    public void FadeOut(float fade_time = 0.5f)
    {
        fade_scr.enabled = true;
        fade_scr.color = new Vector4(0, 0, 0, 0);
        fade_scr.DOFade(1, fade_time);
    }

   // public void FadeInOut_group(CanvasGroup group)
   // {
   //     group.DOFade(1, fade_time).OnComplete(() =>
   //      FadeOut_group(group));
   // }

    public void FadeIn_group(CanvasGroup group, float fade_time = 0.5f)
    {
        group.DOFade(1, fade_time);
    }

    public void FadeOut_group(CanvasGroup group, float fade_time = 0.5f)
    {
        group.DOFade(0, fade_time);
    }
}

