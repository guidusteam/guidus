﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Bullet : MonoBehaviour {

    public Transform shoot_point;
    Player_State player_state;
    MeshRenderer draw;
    Collider collider;
    TrailRenderer trail;

    void Start()
    {
        draw = this.gameObject.GetComponent<MeshRenderer>();
        collider = this.gameObject.GetComponent<Collider>();
        draw.enabled = false;
        collider.enabled = false;
        player_state = GameObject.Find("Player").GetComponent<Player_State>();
        trail = GetComponent<TrailRenderer>();
    }

    void OnTriggerEnter(Collider other)
    {
        //적이나 장애물에 충돌하면 제자리로, 
        //총알끼리, 문에 충돌하면 무시!
        if (other.CompareTag("Obstacle") || other.CompareTag("Enemy") || other.CompareTag("Collider_Wall") || other.CompareTag("Floor"))
            Reset();
    }


    public void Reset()
    {
        draw.enabled = false;
        collider.enabled = false;
        trail.enabled = false;
        this.transform.DOKill();
        this.transform.position = shoot_point.position;
    }
}
