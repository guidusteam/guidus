﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {

    public bool is_active;
    public NPCType mytype;

    GameObject word_bubble;

    TextMesh word;

    Player_Animate player_animate;

    string[] ment = {
        "A_1","A_2","A_3","A_4","A_5",
        "안녕! 반가워","안녕! 반가워","안녕! 반가워","안녕! 반가워","안녕! 반가워",
        "난 대장장이 스미스야","난 대장장이 스미스야","난 대장장이 스미스야","난 대장장이 스미스야","난 대장장이 스미스야",
        "D_1","D_2","D_3","D_4","D_5",
        "E_1","E_2","E_3","E_4","E_5",
    };

    void Start()
    {
        player_animate = GameObject.Find("Player").GetComponent<Player_Animate>();

        word_bubble = GameObject.Find("word_bubble" + (int)mytype);
        word = word_bubble.GetComponentInChildren<TextMesh>();

        word_bubble.SetActive(false);
    }

    void OnMouseDown()
    {
        player_animate.dest_pos = this.transform.position;
        player_animate.FindPath();
    }

    public IEnumerator ShowMent()
    {
        for(int i = (int)mytype * Define.NPC_MENT; i < (int)(mytype + 1) * Define.NPC_MENT; ++i)
        {
            float time = Random.Range(2.0f, 7.0f);
            yield return new WaitForSeconds(time);

            word_bubble.SetActive(true);
            word.text = ment[i];

            time = Random.Range(3.0f, 6.0f);
            yield return new WaitForSeconds(time);

            word_bubble.SetActive(false);
        }
        StartCoroutine("ShowMent");
    }
}

