﻿using UnityEngine;
using System.Collections;

//Grid manager class handles all the grid properties
public class GridManager : MonoBehaviour
{
    // s_Instance is used to cache the instance found in the scene so we don't have to look it up every time.
    private static GridManager s_Instance = null;

    // This defines a static instance property that attempts to find the manager object in the scene and
    // returns it to the caller.
    public static GridManager instance
    {
        get
        {
            if (s_Instance == null)
            {
                // This is where the magic happens.
                //  FindObjectOfType(...) returns the first GridManager object in the scene.
                s_Instance = FindObjectOfType(typeof(GridManager)) as GridManager;
                if (s_Instance == null)
                    Debug.Log("Could not locate an GridManager object. \n You have to have exactly one GridManager in the scene.");
            }
            return s_Instance;
        }
    }

    // Ensure that the instance is destroyed when the game is stopped in the editor.
    void OnApplicationQuit()
    {
        s_Instance = null;
    }

    #region Fields
    public int numOfRows;
    public int numOfColumns;
    public float gridCellSize;
    public bool showGrid = true;
    public bool showObstacleBlocks = true;

    private Vector3 origin = new Vector3();
    public Player_Animate player_animate;
    public GameObject[] obstacleList;
    public Node[,] nodes { get; set; }
    public Node[,] obs_nodes { get; set; }
    #endregion

    //Origin of the grid manager
    public Vector3 Origin
    {
        get { return origin; }
    }

    //Initialise the grid manager
	void CreateObstacles()
	{
		print("장애물");
		//Get the list of obstacles objects tagged as "Obstacle"
        //obstacleList.
		obstacleList = GameObject.FindGameObjectsWithTag("Obstacle");
		CalculateObstacles();
	}

   void InitNodes()
    {
        //Initialise the nodes
        nodes = new Node[numOfRows, numOfColumns];

        int index = 0;
        for (int i = 0; i < numOfRows; i++)
        {
            for (int j = 0; j < numOfColumns; j++)
            {
                Vector3 cellPos = GetGridCellCenter(index);
                Node node = new Node(cellPos);
                nodes[i, j] = node;

                index++;
            }
        }

    }
    /// <summary>
    /// Calculate which cells in the grids are mark as obstacles
    /// </summary>
    void CalculateObstacles()
    {
       
        //Run through the bObstacle list and set the bObstacle position
        if (obstacleList != null && obstacleList.Length > 0)
        {
            foreach (GameObject data in obstacleList)
            {
                int indexCell = GetGridIndex(data.transform.position);
                int col = GetColumn(indexCell);
                int row = GetRow(indexCell);

                //Also make the node as blocked status
                nodes[row, col].MarkObj(ObjType.obstacle);
            }
        }
    }

    public void CalculateObj(Vector3 pos, ObjType type)
    {
        int idxCell = GetGridIndex(pos);
        int col = GetColumn(idxCell);
        int row = GetRow(idxCell);


        //비어있을 경우만 type 변경
        if (nodes[row, col].objtype != ObjType.empty)
            return;

        nodes[row, col].MarkObj(type);
    }
    public void CalculateFlr(Vector3 pos, FlrType type)
    {
        int idxCell = GetGridIndex(pos);
        int col = GetColumn(idxCell);
        int row = GetRow(idxCell);

        nodes[row, col].MarkFlr(type);
    }

    void ResetObjNode(Vector3 pos)
    {

        if(pos == new Vector3(-1, -1, -1))  //모든 노드 리셋
        {
            foreach(Node node in nodes){
                if (node.objtype != ObjType.empty)
                    ResetObjNode(node.position);
            }
            return;
        }

        int idxCell = GetGridIndex(pos);
        int col = GetColumn(idxCell);
        int row = GetRow(idxCell);

        nodes[row, col].MarkObj(ObjType.empty);
    }

    void ResetFlrNode(Vector3 pos)
    {
        int idxCell = GetGridIndex(pos);
        int col = GetColumn(idxCell);
        int row = GetRow(idxCell);

        nodes[row, col].MarkFlr(FlrType.floor);
    }

    /// <summary>
    /// Returns position of the grid cell in world coordinates
    /// </summary>
    /// 
    public Vector2 GetNodeRowCol(Vector3 pos)
    {
        int idxCell = GetGridIndex(pos);
        int col = GetColumn(idxCell);
        int row = GetRow(idxCell);

        return new Vector2(row, col);
    }

    public Vector3 GetGridCellCenter(int index)
    {
        Vector3 cellPosition = GetGridCellPosition(index);
        cellPosition.x += (gridCellSize / 2.0f);
        cellPosition.z += (gridCellSize / 2.0f);

        return cellPosition;
    }

    /// <summary>
    /// Returns position of the grid cell in a given index
    /// </summary>
    public Vector3 GetGridCellPosition(int index)
    {
        int row = GetRow(index);
        int col = GetColumn(index);
        float xPosInGrid = row * gridCellSize;
        float zPosInGrid = col * gridCellSize;

        return Origin + new Vector3(xPosInGrid, 0.0f, zPosInGrid);
    }

    /// <summary>
    /// Get the grid cell index in the Astar grids with the position given
    /// </summary>
    public int GetGridIndex(Vector3 pos)
    {
        if (!IsInBounds(pos))
        {
            return -1;
        }

        pos -= Origin;

        int col = (int)(pos.z / gridCellSize);
        int row = (int)(pos.x / gridCellSize);

        return (row * numOfColumns + col);
    }

    /// <summary>
    /// Get the row number of the grid cell in a given index
    /// </summary>
    public int GetRow(int index)
    {
        int row = index / numOfColumns;
        return row;
    }

    /// <summary>
    /// Get the column number of the grid cell in a given index
    /// </summary>
    public int GetColumn(int index)
    {
        int col = index % numOfColumns;
        return col;
    }

    /// <summary>
    /// Check whether the current position is inside the grid or not
    /// </summary>
    public bool IsInBounds(Vector3 pos)
    {
        float width = numOfRows * gridCellSize;
        float height = numOfColumns * gridCellSize;

        return (pos.x >= Origin.x &&  pos.x <= Origin.x + width && pos.z <= Origin.z + height && pos.z >= Origin.z);
    }
		

    /// <summary>
    /// Get the neighour nodes in 8 different directions
    /// </summary>
    public void GetNeighbours(Node node, ArrayList neighbors)
    {
        Vector3 neighborPos = node.position;
        int neighborIndex = GetGridIndex(neighborPos);
        int row = GetRow(neighborIndex);
        int column = GetColumn(neighborIndex);

        //Top
        int leftNodeRow = row + 1;
        int leftNodeColumn = column;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Top Right
        leftNodeRow = row + 1;
        leftNodeColumn = column + 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Right
        leftNodeRow = row;
        leftNodeColumn = column + 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Bottom Right
        leftNodeRow = row - 1;
        leftNodeColumn = column + 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Bottom
        leftNodeRow = row - 1;
        leftNodeColumn = column;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Bottom Left
        leftNodeRow = row - 1;
        leftNodeColumn = column - 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Left
        leftNodeRow = row;
        leftNodeColumn = column - 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Top Left
        leftNodeRow = row + 1;
        leftNodeColumn = column - 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);
    }
	
	/// <summary>
	/// Check the neighbour. If it's not obstacle, assigns the neighbour.
	/// </summary>
	/// <param name='row'>
	/// Row.
	/// </param>
	/// <param name='column'>
	/// Column.
	/// </param>
	/// <param name='neighbors'>
	/// Neighbors.
	/// </param>
    void AssignNeighbour(int row, int column, ArrayList neighbors)
    {
        
        if (row != -1 && column != -1 && row < numOfRows && column < numOfColumns)
        {
            Node nodeToAdd = nodes[row, column];

            //빈 노드만 길찾기 할 수 있게 한다. 적, NPC, 장애물 X
            if (nodeToAdd.objtype == ObjType.empty && nodeToAdd.flrtype == FlrType.floor)
                neighbors.Add(nodeToAdd);

            else if (nodeToAdd.objtype == ObjType.character)
                neighbors.Add(nodeToAdd);

            //문은 들어가고자 할 때만
            else if (nodeToAdd.flrtype == FlrType.door && player_animate.dest_pos == nodeToAdd.position)
                neighbors.Add(nodeToAdd);
            //caravan 출구
            else if (nodeToAdd.flrtype == FlrType.exit && player_animate.dest_pos == nodeToAdd.position)
                neighbors.Add(nodeToAdd);
            //NPC 터치
            else if (nodeToAdd.objtype == ObjType.NPC && player_animate.dest_pos == nodeToAdd.position)
                neighbors.Add(nodeToAdd);
        } 
    }

    /// <summary>
    /// Show Debug Grids and obstacles inside the editor
    /// </summary>
    void OnDrawGizmos()
    {
        //Draw Grid
        if (showGrid)
        {
            DebugDrawGrid(transform.position, numOfRows, numOfColumns, gridCellSize, Color.blue);
        }

        //Grid Start Position
        Gizmos.DrawSphere(transform.position, 0.5f);

        //Draw Obstacle obstruction
        if (showObstacleBlocks)
        {
            Vector3 cellSize = new Vector3(gridCellSize, 1.0f, gridCellSize);

            if (obstacleList != null && obstacleList.Length > 0)
            {
                foreach (GameObject data in obstacleList)
                {
                    Gizmos.DrawCube(GetGridCellCenter(GetGridIndex(data.transform.position)), cellSize);
                }
            }
        }
        
      // foreach(Node node in nodes)
      // {
      //     Vector3 cellSize = new Vector3(gridCellSize*0.5f, 1.0f, gridCellSize*0.5f);
      //     if (node.flrtype == FlrType.door)
      //         Gizmos.DrawCube(GetGridCellCenter(GetGridIndex(node.position)), cellSize);
      // }
    }

    /// <summary>
    /// Draw the debug grid lines in the rows and columns order
    /// </summary>
    public void DebugDrawGrid(Vector3 origin, int numRows, int numCols, float cellSize, Color color)
    {
        float width = (numRows * cellSize);
        float height = (numCols * cellSize);

        // Draw the horizontal grid lines
        for (int i = 0; i < numCols + 1; i++)
        {
            Vector3 startPos = origin + i * cellSize * new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 endPos = startPos + width * new Vector3(1.0f, 0.0f, 0.0f);
            Debug.DrawLine(startPos, endPos, color);
        }

        // Draw the vertial grid lines
        for (int i = 0; i < numRows + 1; i++)
        {
            Vector3 startPos = origin + i * cellSize * new Vector3(1.0f, 0.0f, 0.0f);
            Vector3 endPos = startPos + height * new Vector3(0.0f, 0.0f, 1.0f);
            Debug.DrawLine(startPos, endPos, color);
        }
    }
}
