﻿using UnityEngine;
using System;
using System.Collections;

public enum Dir { left, right, up, down };
public enum ObjType { empty, obstacle, enemy, character, NPC };
public enum FlrType { floor, door, exit };
public enum NPCType { A, B, C, D, E };
public enum ItemType { max_hp_inc, att_power_inc, move_spd_inc, max_stam_inc };
public enum DItemType { hp, coin };

public class Define {
    
    public const int MAPSIZE_W = 63;
    public const int MAPSIZE_H = 28;
    
    //맵 좌표 범위 0부터
    public const int HMAX = 44;     //가로 
    public const int VMAX = 24;     //세로 

    //맵 중앙
    public const int HCENTER = HMAX / 2 + 1;
    public const int VCENTER = VMAX / 2 + 1;

    public const int C_POS_Y = 50; //카메라 기본 y좌표
    public const int C_POS_Z = -50; //카메라 기본 z좌표

    //카메라가 플레이어를 따라 이동하는 범위
    public const int C_HMAX = 33;   //가로 최대
    public const int C_HMIN = 11;   //가로 최소
    public const int C_VMAX = 17;   //세로 최대
    public const int C_VMIN = 11;   //세로 최소

    //플레이어 속성 기본
    public const int MAX_HEALTH = 100, MAX_STAMINA = 30;
    public const float MOVE_SPEED = 0.07f, SHOOT_SPEED = 0.8f;
    public const int ATTACK_POWER = 5;
    public const int ATTACK_BOUND = 3;

    //아이템
    public const int ITEM_NUM = 4;
    public const int DITEM_NUM = 2;
    
    //탄환 각도


    //부활할때 물려받는 percent 최대값
    public const int MAX_PERCENT = 50;

    public const int ROLL_COST = 100;

    //npc가 구사하는 멘트 수
    public const int NPC_MENT = 5;

    //한 층에 배치되는 방 갯수
    public const int ROOM_NUM = 10;

    public static Vector3[] door_pos;
    public const int door_count = 4;

    public static void Setting()
    {
        door_pos = new Vector3[door_count*2];
        door_pos[(int)Dir.left] = new Vector3(3, 0, VCENTER);
        door_pos[(int)Dir.right] = new Vector3(HMAX - 3, 0, VCENTER);
        door_pos[(int)Dir.up] = new Vector3(HCENTER, 0, VMAX - 3);
        door_pos[(int)Dir.down] = new Vector3(HCENTER, 0, 3);

        //문 주변
        door_pos[door_count + (int)Dir.left] = new Vector3(1, 0, VCENTER);
        door_pos[door_count + (int)Dir.right] = new Vector3(HMAX - 1, 0, VCENTER);
        door_pos[door_count + (int)Dir.up] = new Vector3(HCENTER, 0, VMAX - 1);
        door_pos[door_count + (int)Dir.down] = new Vector3(HCENTER, 0, 1);
    }
}
