﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    MeshRenderer item_mesh;
    BoxCollider item_coll;
    public string type;
    int room_num;

    void Start() {
        item_mesh = this.GetComponent<MeshRenderer>();
        item_coll = this.GetComponent<BoxCollider>();
        Reset();
    }

    public void SetPos(Vector3 pos)
    {
        this.transform.position = pos;
    }

    public void SetRoom(int num)
    {
        room_num = num;
    }

    public void Reset()
    {
        room_num = -1;
        Hide();
    }

    public bool FindByNum(int num = -1)
    {
        if (room_num == num) return true;
        return false;
    }

    public bool FindByPos(Vector3 pos)
    {
        if (this.transform.position == pos) return true;
        return false;
    }

    public void Show()
    {
        item_mesh.enabled = true;
        item_coll.enabled = true;
    }

    public void Hide()
    {
        item_mesh.enabled = false;
        item_coll.enabled = false;
    }
}
