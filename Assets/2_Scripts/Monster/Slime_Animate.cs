﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Slime_Animate : MonoBehaviour 
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;

    GameObject player;
    Player_State player_state;
    Animator slime_action;
    Set_Object set_obj;
    Item_Manager item_mng;

    float move_speed = 0.4f;
    bool attack = false;
    int attack_power = 3;

	// Use this for initialization
    void SetTarget()
    {
        player = GameObject.FindGameObjectWithTag("Player_Character");
        player_state = player.GetComponent<Player_State>();
        slime_action = this.gameObject.GetComponent<Animator>();
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
        
    }


    void FindPath()
    {
        startPos = this.transform;
        endPos = player.transform;
        endPos.position = new Vector3(player.transform.position.x, 0, player.transform.position.z);
        
        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

        if (pathArray.Count == 2) StartCoroutine("Attack");
        else if (pathArray.Count < 2) StartCoroutine("Break");
        else Move();
            

    }

    void Move()
    {
        attack = false;
        slime_action.SetBool("attack", false);
        slime_action.SetBool("move", true);
        Node prev = (Node)pathArray[0];
        Node node = (Node)pathArray[1];
        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", prev.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, move_speed);

        this.transform.DOMove(node.position, move_speed).SetEase(Ease.InOutCubic).OnComplete(() => StartCoroutine("Break"));  
        
    }

    IEnumerator Break()
    {
        slime_action.SetBool("move", false);

        yield return new WaitForSeconds(0.8f);

        if(!attack) FindPath();
    }

    IEnumerator Attack()
    {
        //print("공격"+Time.time);
        attack = true;
        slime_action.SetBool("move", false);
        slime_action.SetBool("attack", true);

        Quaternion look_rot = Quaternion.LookRotation(player.transform.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.6f);

        //거리 안에 있으면
        if ((player.transform.position - this.transform.position).magnitude < Define.ATTACK_BOUND)
        {
            player_state.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.4f);

        FindPath();
    }

    IEnumerator Die()
    {
        slime_action.SetBool("die", true);
        yield return new WaitForSeconds(0.6f);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);

        slime_action.SetBool("die", false);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(transform.position);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}