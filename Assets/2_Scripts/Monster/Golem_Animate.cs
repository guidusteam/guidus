﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Golem_Animate : MonoBehaviour
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
    

    GameObject player;
    Player_State player_state;
    Animator golem_action;

    float move_speed = 0.4f;
    int attack_power = 10;

    // Use this for initialization

    void SetTarget()
    {
        player = GameObject.FindGameObjectWithTag("Player_Character");
        player_state = player.GetComponent<Player_State>();
        golem_action = this.gameObject.GetComponent<Animator>();
        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
    }


    void FindPath()
    {
        startPos = this.transform;
        endPos = player.transform;

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);


        if (pathArray.Count == 3) StartCoroutine("Attack");
        else if (pathArray.Count > 3) Move();
        else if (pathArray.Count < 3) StartCoroutine("Break");
    }

    void Move()
    {
       golem_action.SetBool("walk", true);
       Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        this.transform.DOLocalMove(node.position, move_speed).SetEase(Ease.InOutCubic).OnComplete(() => FindPath());

       //회전값
       Quaternion look_rot = Quaternion.LookRotation(node.position - this.transform.localPosition);
       Vector3 dest_rot = look_rot.eulerAngles;
       this.transform.DORotate(dest_rot, move_speed);
    }

    IEnumerator Attack()
    {
        golem_action.SetBool("walk", false);
        golem_action.SetBool("attack", true);

        Quaternion look_rot = Quaternion.LookRotation(player.transform.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.8f);

        //거리 안에 있으면
        if ((player.transform.position - this.transform.position).magnitude < Define.ATTACK_BOUND)
        {
            player_state.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.2f);

        StartCoroutine("Break");
    }

    IEnumerator Break()
    {
        golem_action.SetBool("attack", false);
        golem_action.SetBool("walk", false);

        yield return new WaitForSeconds(1.5f);

        FindPath();
    }

    IEnumerator Die()
    {
        golem_action.CrossFade("die", 0.3f);
        yield return new WaitForSeconds(1.5f);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);

        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}