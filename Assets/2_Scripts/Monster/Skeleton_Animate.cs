﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Skeleton_Animate : MonoBehaviour {

    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;

    GameObject player;
    Player_State player_state;
    Set_Object set_obj;
    Item_Manager item_mng;
    Animation skel_anim;

    int move_count = 0;
    float move_speed = 0.5f;
    bool attack = false;
    int attack_power = 7;

    // Use this for initialization
    void SetTarget()
    {
        player = GameObject.Find("Player");
        player_state = player.GetComponent<Player_State>();
        skel_anim = GetComponent<Animation>();
        set_obj = GetComponentInParent<Set_Object>();
        item_mng = GameObject.Find("Items").GetComponent<Item_Manager>();

        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();

    }


    void FindPath()
    {
        if (move_count >= 3 && !attack)
        {    //3칸 움직이고 한번 휴식
            this.transform.DOKill();
            StartCoroutine("Break");
            return;
        }


        startPos = this.transform;
        endPos = player.transform;
        endPos.position = new Vector3(player.transform.position.x, 0, player.transform.position.z);

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);

        if (pathArray.Count <= 2) StartCoroutine("Attack");
        else Move();

    }

    IEnumerator Break()
    {       
        skel_anim.CrossFade("idle");
        pathArray.Clear();

        yield return new WaitForSeconds(3.0f);

        move_count = 0;
        FindPath();
    }

    void Move()
    {
        attack = false;

        //현재 move동작이 아닐때만 새로 재생
        if (!skel_anim.IsPlaying("move"))
            skel_anim.CrossFade("move");
        Node node = (Node)pathArray[1];

        //노드 이동
        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        GridManager.instance.CalculateObj(node.position, ObjType.enemy);

        this.transform.DOMove(node.position, move_speed).SetEase(Ease.Linear).OnComplete(() => FindPath());

        //회전값
        Quaternion look_rot = Quaternion.LookRotation(node.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, move_speed);
        move_count++;
    }

    IEnumerator Attack()
    {
        attack = true;

        if (!skel_anim.IsPlaying("attack"))
            skel_anim.CrossFade("attack");
        move_count = 0;

        Quaternion look_rot = Quaternion.LookRotation(player.transform.position - this.transform.position);
        Vector3 dest_rot = look_rot.eulerAngles;
        this.transform.DORotate(dest_rot, 0.2f);

        yield return new WaitForSeconds(0.5f);

        //거리 안에 있으면
        if ((player.transform.position - this.transform.position).magnitude < Define.ATTACK_BOUND)
        {
            player_state.Update_hp(-attack_power);
        }

        yield return new WaitForSeconds(0.5f);

        FindPath();
    }

    IEnumerator Die()
    {
        skel_anim.CrossFade("die");
        yield return new WaitForSeconds(0.9f);

        GridManager.instance.SendMessage("ResetObjNode", transform.position);
        set_obj.CheckEnemy(-1);
        int rand = Random.Range(0, 10000);
        if (rand < 1000)
            item_mng.DropItem(transform.position);
        this.gameObject.SetActive(false);
    }

    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}
