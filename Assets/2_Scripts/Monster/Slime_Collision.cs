﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Slime_Collision : MonoBehaviour {

    public float health = 20;
    Player_State Player_State;
    Slime_Animate slime_animate;
    float damage;

    void Start()
    {
        slime_animate = GetComponent<Slime_Animate>();
        Player_State = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.tag == "Bullet")
        {
            damage = Player_State.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = 20;
        this.transform.DOKill();
        slime_animate.StartCoroutine("Die");
    }
}