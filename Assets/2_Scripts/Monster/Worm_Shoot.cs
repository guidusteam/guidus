﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Worm_Shoot: MonoBehaviour {
    
    public int bullet_damage = 3;
    public float bullet_speed = 0.3f;
    public int bullet_dist =40;
    public Transform player_trans;
    public Transform worm_trans;
    public GameObject[] bullet;
    Worm_Bullet[] bullet_scrt ;
    public MeshRenderer[] bullet_draw;
    public Collider[] bullet_coll;
    Vector3[] dest_pos;
    Vector3 player_pos;

    void Start()
    {
        bullet_scrt = new Worm_Bullet[bullet.Length];
        bullet_draw = new MeshRenderer[bullet.Length];
        bullet_coll = new Collider[bullet.Length];
        dest_pos = new Vector3[bullet.Length];

        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet_scrt[i] = bullet[i].GetComponent<Worm_Bullet>();
            bullet_draw[i] = bullet[i].GetComponent<MeshRenderer>();
            bullet_coll[i] = bullet[i].GetComponent<Collider>();
        }
    }

    public IEnumerator ShootBullet_2way()
    {
        yield return new WaitForSeconds(0.5f);


        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet_draw[i].enabled = true;
            bullet_coll[i].enabled = true;
        }
        //발사 목적지 지정
        dest_pos[0] = this.transform.position + Vector3.right * bullet_dist;
        dest_pos[1] = this.transform.position + Vector3.left * bullet_dist;

        //벽 넘어가지 않게 목표지점 조절
        //양쪽으로 발사
        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet[i].transform.DOMove(dest_pos[i], bullet_speed).SetEase(Ease.Linear);
        }
        yield return new WaitForSeconds(bullet_speed);

        for (int i = 0; i < bullet.Length; ++i)
            bullet_scrt[i].Reset();


    }

    public IEnumerator ShootBullet_4way()
    {
        yield return new WaitForSeconds(0.5f);


        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet_draw[i].enabled = true;
            bullet_coll[i].enabled = true;
        }
        //발사 목적지 지정
        dest_pos[0] = this.transform.position + Vector3.right * bullet_dist;
        dest_pos[1] = this.transform.position + Vector3.left * bullet_dist;
        dest_pos[2] = this.transform.position + Vector3.forward * bullet_dist;
        dest_pos[3] = this.transform.position + Vector3.back * bullet_dist;

        //4방향으로 발사
        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet[i].transform.DOMove(dest_pos[i], bullet_speed).SetEase(Ease.Linear);
        }

        yield return new WaitForSeconds(bullet_speed);

        for (int i = 0; i < bullet.Length; ++i)
            bullet_scrt[i].Reset();
    }

    public IEnumerator ShootBullet_chasing()
    {
        //플레이어 좌표 받아오기
        player_pos = player_trans.position;

        //쏘는 방향으로 회전
        Quaternion look_rot = Quaternion.LookRotation(player_pos - worm_trans.position);
        Vector3 dest_rot = look_rot.eulerAngles; 


        worm_trans.DORotate(dest_rot, 0.3f);


        yield return new WaitForSeconds(0.5f);


        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet_draw[i].enabled = true;
            bullet_coll[i].enabled = true;
        }



        //발사 벡터구하고 정규화
        Vector3 shoot_vector = player_pos - worm_trans.position;
        shoot_vector.Normalize();

        //발사 방향으로 dist만큼 , 약간의 방향 변경
        dest_pos[0] = worm_trans.position + shoot_vector * bullet_dist + Vector3.up * 1.5f;
        dest_pos[1] = worm_trans.position + shoot_vector * bullet_dist + Vector3.up;
        dest_pos[2] = worm_trans.position + shoot_vector * bullet_dist + Vector3.up * 0.5f;


        //각자 방향으로 약간 다른 속도로 발사
        for (int i = 0; i < bullet.Length; ++i)
        {
            bullet[i].transform.DOMove(dest_pos[i], bullet_speed - i * 0.05f).SetEase(Ease.Linear);
        }

        yield return new WaitForSeconds(bullet_speed);

        for (int i = 0; i < bullet.Length; ++i)
            bullet_scrt[i].Reset();
    }
}