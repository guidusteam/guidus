﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Golem_Collision : MonoBehaviour {
    public int set_health;
    public float health;
    Player_State player_state;
    Golem_Animate golem_animate;
    float damage;

    void Start()
    {
        health = set_health;
        golem_animate = GetComponent<Golem_Animate>();
        player_state = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();
        damage = player_state.attack_power;
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.tag == "Bullet")
        {
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = set_health;
        this.transform.DOKill();
        golem_animate.StartCoroutine("Die");
    }
}
