﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Skeleton_Collision : MonoBehaviour {

    public float health = 30;
    Player_State player_state;
    Skeleton_Animate skell_animate;

    float damage;

    void Start()
    {
        player_state = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();
        skell_animate = GetComponent<Skeleton_Animate>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.tag == "Bullet")
        {
            damage = player_state.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = 30;
        this.transform.DOKill();
        skell_animate.StartCoroutine("Die");
    }
}
