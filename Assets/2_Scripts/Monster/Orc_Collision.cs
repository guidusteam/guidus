﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Orc_Collision : MonoBehaviour
{

    public float health = 30;
    Player_State player_state;
    Orc_Animate orc_animate;

    float damage;

    void Start()
    {
        player_state = GameObject.FindGameObjectWithTag("Player_Character").GetComponent<Player_State>();
        orc_animate = GetComponent<Orc_Animate>();
    }

    void OnTriggerEnter(Collider other)
    {
        //총알에 맞았을 때
        if (other.tag == "Bullet")
        {
            damage = player_state.attack_power;
            health -= damage;
            if (this.health <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        health = 30;
        this.transform.DOKill();
        orc_animate.StartCoroutine("Die");
    }
}
